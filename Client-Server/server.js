var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');

var users = 0;

app.get('/', function(req, res){
  res.sendFile(__dirname + '/chat.html');
});

function logUsers(num){
	users =  users + num;
	if (users === 1)
	{
		var userData = "There is " + users + " client connected";
	}

	if (users < 1)
	{
		var userData = "There are no clients connected";
	}

	if (users > 1)
	{
		var userData = "There are " + users + " clients connected";
	}
	
  	fs.appendFile('log.txt', userData + "\n");
  	console.log(userData);
}

io.on('connection', function(socket){
  socket.on('new user', function(){
  	logUsers(1);
  });
  socket.on('disconnect', function(){
  	logUsers(-1);
  });
  socket.on('chat message', function(name, msg){
  	message = name + " says: " + msg;
    io.emit('chat message', message);
  });
  socket.on('ping', function() {
    socket.emit('pong');
  });
  socket.on('ping result', function(pingname, latency) {
  	var ip = socket.request.connection.remoteAddress;
  	var data = "the ping of the user " + pingname + " with ip: "+ ip + " is: " + latency + "ms";
  	fs.appendFile('log.txt', data + "\n");
    console.log(data);
  });
});

http.listen(80, function(){
  console.log('listening on port 80');
});