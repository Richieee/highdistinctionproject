var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require("body-parser");
var ExpressPeerServer = require('peer').ExpressPeerServer;
var server = app.listen(80);

var options = {
	debug: true
}

app.get('/', function (req, res) {
  res.sendFile( __dirname + "/chat.html");
});

app.use('/', ExpressPeerServer(server, options));